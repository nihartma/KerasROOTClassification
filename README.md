# KerasROOTClassification

This is an attempt to simplify the training of Keras models from ROOT TTree input.

The recommended usage is to put this module in your python path and
create run scripts to define and train your model.

For example:

```python
import numpy as np
import logging

from KerasROOTClassification import ClassificationProject

logging.basicConfig()
logging.getLogger("KerasROOTClassification").setLevel(logging.INFO)

c = ClassificationProject("my_project", # this will also be the name of the project directory
                          signal_trees = [(filename1, treename1)],
                          bkg_trees = [(filename2, treename2),
                                       (filename3, treename3),
                          ],
                          optimizer="Adam",
                          selection="some-selection-expression",
                          branches = ["var1", "var2", "var3"],
                          weight_expr = "some-weight-expression",
                          identifiers = ["var4", "var5"], # variables that identify which events were used for training
                          step_bkg = 10, # take every 10th bkg event for training
                          step_sig = 2, # take every second sig event for training
)

c.train(epochs=20)
```

Previously created projects can be inspected in iypthon like

```
ipython -i -m KerasROOTClassification.browse <project-dir>
```

Or in a script you can initialise a project by just specifying the path to the project directory. This is especially useful when you want to compare different projects:

```python
from KerasROOTClassification import ClassificationProject
from KerasROOTClassification.compare import overlay_ROC, overlay_loss

c1 = ClassificationProject("path/to/project1")
c2 = ClassificationProject("path/to/project2")

overlay_ROC("ROC_overlay.pdf", c1, c2)
overlay_loss("loss_overlay.pdf", c1, c2)
```

# Conda setup

An example for a mini conda setup that contains the nescessary packages:

```sh
conda install keras pandas matplotlib scikit-learn pydot graphviz jupyter
pip install root_numpy
```
